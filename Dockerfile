FROM php:8.1

COPY fcv.phar /app/fcv.phar

WORKDIR /app

CMD ["php", "/app/fcv.phar"]